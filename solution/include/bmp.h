#ifndef BMP_H
#define BMP_H
#include "image.h"
#include "status.h"
#define BMP_TYPE 0x4D42
#define BIT_COUNT 24
#define BMP_RESERVED 0
#define SIZE 40
#define PLANES 1
#define COMPRESSION 0
#define X_PELS_PER_METER 0
#define Y_PELS_PER_METER 0
#define CLR_USED 0
#define CLR_IMPORTANT 0

#pragma pack(push, 1)
struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)


enum read_status from_bmp( FILE* in, struct image* img);
struct bmp_header get_header(const struct image* img);
enum write_status to_bmp(FILE* out, const struct image* img);
#endif
