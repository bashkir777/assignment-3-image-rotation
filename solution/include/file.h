#ifndef FILE_H
#define FILE_H

enum fopen_status{
    OPEN_OK=0,
    OPEN_ERROR,
};

enum fclose_status{
    CLOSE_OK=0,
    CLOSE_ERROR
};
enum fclose_status close_file(FILE* file_pointer);
enum fopen_status open_file_write(FILE** file_pointer, char* filename);
enum fopen_status open_file_read(FILE** file_pointer, char* filename);

#endif
