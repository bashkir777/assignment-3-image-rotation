#ifndef ROTATE_H
#define ROTATE_H
struct image rotate_clockwise_90(struct image source);
struct image rotate(struct image source, uint16_t angle);
#endif
