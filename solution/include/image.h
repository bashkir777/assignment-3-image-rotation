#ifndef IMAGE_H
#define IMAGE_H
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#pragma pack(push, 1)
struct pixel {
    uint8_t b;
    uint8_t g; 
    uint8_t r; 
};
#pragma pack(pop)
struct image {
    uint64_t width;
    uint64_t height;  
    struct pixel* data; 
};
struct image create_image(uint64_t width, uint64_t height);
void free_img(struct image* img);
#endif
