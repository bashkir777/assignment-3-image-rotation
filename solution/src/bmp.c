#include "../include/bmp.h"
#include "../include/image.h"
#define MAX_PADDING 4


size_t calculate_padding(uint32_t width){
    return (MAX_PADDING - (width * sizeof(struct pixel)) % MAX_PADDING) % MAX_PADDING;
}


enum read_status from_bmp( FILE* in, struct image* img ){
    struct bmp_header header;
    if (fread(&header, sizeof(header), 1, in) != 1) {
       
        return READ_INVALID_HEADER;
    }
    
    if (header.biBitCount != BIT_COUNT || header.bfType != BMP_TYPE) {
        return READ_INVALID_SIGNATURE;
    }
    *img = create_image((uint64_t)header.biWidth, (uint64_t) header.biHeight);
    
    uint32_t padding = calculate_padding(header.biWidth);
    uint32_t read = 0;
    
    while (read < header.biHeight) {
        if (fread(img->data + read * img->width, sizeof(struct pixel), header.biWidth, in) != img->width) {
            free_img(img);
            return READ_INVALID_BITS;
        }
        if(fseek(in, (long)padding, SEEK_CUR)!=0){
            free_img(img);
            return READ_INVALID_BITS;
        }
        read++;
    }
    return READ_OK;
}

struct bmp_header get_header(const struct image* img){
    struct bmp_header header;
    header.bfType = BMP_TYPE;
    header.bfReserved = BMP_RESERVED;
    header.bOffBits = sizeof(header);
    header.biSize = SIZE;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = PLANES;
    header.biBitCount = BIT_COUNT;
    header.biCompression = COMPRESSION;
    header.biXPelsPerMeter = X_PELS_PER_METER;
    header.biYPelsPerMeter = Y_PELS_PER_METER;
    header.biClrUsed = CLR_USED;
    header.biClrImportant = CLR_IMPORTANT;
    size_t padding = calculate_padding(img->width);
    header.bfileSize = sizeof(header) + (img->width * sizeof(struct pixel) + padding) * img->height;
    header.biSizeImage = (img->width * sizeof(struct pixel) + padding) * img->height;
    return header;
}


enum write_status to_bmp(FILE* out, const struct image* img) {
    struct bmp_header header;
    header = get_header(img);
    if (fwrite(&header, sizeof(header), 1, out) != 1) {
        return WRITE_ERROR;
    }
    size_t padding = calculate_padding(img->width);

    for (uint32_t i = 0; i < img->height; ++i) {
        if (fwrite(&img->data[i * img->width], sizeof(struct pixel), img->width, out) != img->width) {
            return WRITE_ERROR;
        }
        if (padding > 0) {
            if (fwrite((uint8_t[3]){0}, 1, padding, out) != padding) {
                return WRITE_ERROR;
            }
        }
    }
    return WRITE_OK;
}


