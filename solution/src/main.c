#include "../include/bmp.h"
#include "../include/file.h"
#include "../include/rotate.h"
#include <inttypes.h>
int main(int argc, char *argv[]) {
    FILE *in;
    FILE *out;
    if (argc!=4){
        printf("input format: <source-image> <transformed-image> <angle>");
    }
    char *source_path = argv[1];
    char *transformed_path = argv[2];

    int32_t angle = (int32_t)strtoimax(argv[3], NULL, 10);
    angle = ((angle % 360) + 360) % 360;
    
    if (open_file_read(&in, source_path) == OPEN_OK && open_file_write(&out, transformed_path) == OPEN_OK){
        struct image source;

        if (from_bmp(in, &source) != READ_OK){
            fprintf(stderr, "не удалось прочитать из bmp файла");
            return 0;
        }
        struct image output;
        output = rotate(source, (uint16_t)angle);
        free_img(&source);
        if(to_bmp(out, &output) != WRITE_OK){
            fprintf(stderr, "не удалось записать в bmp файл");
            return 0;
        }
        free_img(&output);        
        close_file(in);
        close_file(out);
    }else{
        fprintf(stderr, "файла с таким названием не существует");  
        return 0;
    }
}
