#include "image.h"
#include "rotate.h"
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

struct image rotate_clockwise_90(struct image const source) {
    if (source.data != NULL){
        struct image output = create_image(source.height, source.width);
        for (uint32_t y = 0; y < source.height; ++y) {
            for (uint32_t x = 0; x < source.width; ++x) {
                output.data[(output.height - 1 - x) * output.width + y] = source.data[y * source.width + x];
            }
        }
        return output;
    }
    return source;
    
}

void rewrite(struct image *pattern, struct image *toReWrite){
    for (uint64_t i = 0; i < pattern->width * toReWrite->height; i++) {
        toReWrite->data[i] = pattern->data[i];
    }
}

struct image rotate(struct image source, uint16_t angle){
    struct image output;
    struct image temp;

    switch (angle)
    {
    case 90:
        return rotate_clockwise_90(source);
    case 180:
        temp = rotate_clockwise_90(source);
        output = rotate_clockwise_90(temp);
        free_img(&temp);
        return output;
    case 270:
        output = rotate_clockwise_90(source);
        temp = rotate_clockwise_90(output);
        free_img(&output);
        output = rotate_clockwise_90(temp);
        free_img(&temp);
        return output;
    default:
        output = create_image(source.width, source.height);
        rewrite(&source, &output);
        return output;
    }
    return source;
}
