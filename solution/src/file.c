#include "../include/bmp.h"
#include "../include/file.h"
#include "../include/image.h"

enum fopen_status open_file_read(FILE** file_pointer, char* filename){
    *file_pointer = fopen(filename, "rb");
    if (*file_pointer==NULL){
        return OPEN_ERROR;
    }else{
        return OPEN_OK;
    }
}

enum fopen_status open_file_write(FILE** file_pointer, char* filename){
    *file_pointer = fopen(filename, "wb");
    if (*file_pointer==NULL){
        return OPEN_ERROR;
    }else{
        return OPEN_OK;
    }
}


enum fclose_status close_file(FILE* file_pointer){
    if (fclose(file_pointer) == 0) {
        return CLOSE_OK;
    } else {
        return CLOSE_ERROR;
    }
}
