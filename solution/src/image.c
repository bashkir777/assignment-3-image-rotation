#include "image.h"


struct image create_image(uint64_t const width, uint64_t const height){
    struct image img;
    img.height = height;
    img.width = width;
    void* data = malloc(sizeof(struct pixel) * width * height);
    if(data == NULL){
        fprintf(stderr, "Не удалось выделить память под изображение");
    
    }
    img.data = data;
    return img;
}


void free_img(struct image* img){
    if(img != NULL){
        free(img->data);
        img->data = NULL;
    }
}
